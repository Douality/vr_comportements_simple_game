# Projet de Réalité Virtuelle avec Unity

Ce projet a pour objectif une première approche de la réalité virtuelle (VR) en utilisant Unity. Il s'inscrit principalement dans le cadre d'une étude comportementale des joueurs dans un environnement réaliste et de ses effets.

Ce projet établit les bases de la VR, tout en mettant l'accent sur l'importance des feedbacks et des impressions sensorielles dans le contexte d'un jeu, et plus particulièrement en VR. Nous explorons comment les joueurs interagissent avec l'environnement virtuel et comment les sensations immersives influencent leur comportement.

## Objectifs du Projet

1. **Étude Comportementale** : Nous cherchons à comprendre comment les joueurs interagissent avec l'environnement virtuel, comment ils se comportent et réagissent aux stimuli en VR.

2. **Importance des Feedbacks** : Nous mettons en évidence l'importance des feedbacks sensoriels, des retours haptiques et des réponses visuelles pour améliorer l'expérience VR.

3. **Expérience Réaliste** : En mettant en place un environnement réaliste, nous visons à créer une immersion totale pour les joueurs.

4. **Apprentissage** : Nous cherchons à développer de nouvelles compétences dans le domaine de la VR et de l'interaction homme-machine.

## Accès au Projet

Ce projet est hébergé sur GitHub avec lfs, et nous vous invitons à consulter le lien suivant pour en savoir plus : [Lien vers le projet sur GitHub](https://github.com/RKusanali/JankenVR.git).

